User Aggregator allows users to add a syndication feed to a site's aggregator via their user profile.

Features:
  Site administrator can set
   - visible and default categories that will be assigned to user entered feeds. Default categories do not have to be visible
   - default refresh interval for user entered feeds
   - option to show on account setup
   - allow users to select single or multiple categories
  Users can select or unselect visible categories
  User entered feeds go to a pending table for administrative review

Known issues:
  Pure laziness made me use the MySQL-specific REPLACE command in the _user_aggregator_assign_feed() function, so this is not PostgreSQL compatible.

Change log:
Sept 10, 2005: Initial release

